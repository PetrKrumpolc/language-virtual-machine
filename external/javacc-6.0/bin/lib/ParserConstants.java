/* Generated By:JavaCC: Do not edit this line. ParserConstants.java */

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface ParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int VAR = 7;
  /** RegularExpression Id. */
  int INT = 8;
  /** RegularExpression Id. */
  int FLOAT = 9;
  /** RegularExpression Id. */
  int BOOLEAN = 10;
  /** RegularExpression Id. */
  int STRING = 11;
  /** RegularExpression Id. */
  int READ = 12;
  /** RegularExpression Id. */
  int WRITE = 13;
  /** RegularExpression Id. */
  int IF = 14;
  /** RegularExpression Id. */
  int ELSE = 15;
  /** RegularExpression Id. */
  int FOR = 16;
  /** RegularExpression Id. */
  int INTEGER_LITERAL = 17;
  /** RegularExpression Id. */
  int DECIMAL_LITERAL = 18;
  /** RegularExpression Id. */
  int HEX_LITERAL = 19;
  /** RegularExpression Id. */
  int OCTAL_LITERAL = 20;
  /** RegularExpression Id. */
  int FLOATING_POINT_LITERAL = 21;
  /** RegularExpression Id. */
  int EXPONENT = 22;
  /** RegularExpression Id. */
  int BOOLEAN_LITERAL = 23;
  /** RegularExpression Id. */
  int STRING_LITERAL = 24;
  /** RegularExpression Id. */
  int IDENTIFIER = 25;
  /** RegularExpression Id. */
  int LETTER = 26;
  /** RegularExpression Id. */
  int PART_LETTER = 27;
  /** RegularExpression Id. */
  int LPAREN = 28;
  /** RegularExpression Id. */
  int RPAREN = 29;
  /** RegularExpression Id. */
  int LBRACE = 30;
  /** RegularExpression Id. */
  int RBRACE = 31;
  /** RegularExpression Id. */
  int SEMICOLON = 32;
  /** RegularExpression Id. */
  int COMMA = 33;
  /** RegularExpression Id. */
  int ASSIGN = 34;
  /** RegularExpression Id. */
  int GT = 35;
  /** RegularExpression Id. */
  int LT = 36;
  /** RegularExpression Id. */
  int BANG = 37;
  /** RegularExpression Id. */
  int HOOK = 38;
  /** RegularExpression Id. */
  int COLON = 39;
  /** RegularExpression Id. */
  int EQ = 40;
  /** RegularExpression Id. */
  int LE = 41;
  /** RegularExpression Id. */
  int GE = 42;
  /** RegularExpression Id. */
  int NE = 43;
  /** RegularExpression Id. */
  int SC_OR = 44;
  /** RegularExpression Id. */
  int SC_AND = 45;
  /** RegularExpression Id. */
  int PLUS = 46;
  /** RegularExpression Id. */
  int MINUS = 47;
  /** RegularExpression Id. */
  int STAR = 48;
  /** RegularExpression Id. */
  int SLASH = 49;
  /** RegularExpression Id. */
  int REM = 50;
  /** RegularExpression Id. */
  int PLUSASSIGN = 51;
  /** RegularExpression Id. */
  int MINUSASSIGN = 52;
  /** RegularExpression Id. */
  int STARASSIGN = 53;
  /** RegularExpression Id. */
  int SLASHASSIGN = 54;
  /** RegularExpression Id. */
  int REMASSIGN = 55;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\t\"",
    "\"\\n\"",
    "\"\\r\"",
    "\"\\f\"",
    "<token of kind 6>",
    "\"var\"",
    "\"int\"",
    "\"float\"",
    "\"boolean\"",
    "\"String\"",
    "\"read\"",
    "\"write\"",
    "\"if\"",
    "\"else\"",
    "\"for\"",
    "<INTEGER_LITERAL>",
    "<DECIMAL_LITERAL>",
    "<HEX_LITERAL>",
    "<OCTAL_LITERAL>",
    "<FLOATING_POINT_LITERAL>",
    "<EXPONENT>",
    "<BOOLEAN_LITERAL>",
    "<STRING_LITERAL>",
    "<IDENTIFIER>",
    "<LETTER>",
    "<PART_LETTER>",
    "\"(\"",
    "\")\"",
    "\"{\"",
    "\"}\"",
    "\";\"",
    "\",\"",
    "\"=\"",
    "\">\"",
    "\"<\"",
    "\"!\"",
    "\"?\"",
    "\":\"",
    "\"==\"",
    "\"<=\"",
    "\">=\"",
    "\"!=\"",
    "\"||\"",
    "\"&&\"",
    "\"+\"",
    "\"-\"",
    "\"*\"",
    "\"/\"",
    "\"%\"",
    "\"+=\"",
    "\"-=\"",
    "\"*=\"",
    "\"/=\"",
    "\"%=\"",
    "\".\"",
  };

}
