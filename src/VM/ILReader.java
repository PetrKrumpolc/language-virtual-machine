package VM;

import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class ILReader {

    public ArrayList<Pair<String, String>> Read(String fileName){

        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        String line;
        String instruction;
        String arg;
        int separatorIndex;

        try (Scanner scanner = new Scanner(new File(fileName))){

            while (scanner.hasNext()){

                line            = scanner.nextLine();
                separatorIndex  = line.indexOf(" ");
                instruction = line.substring(0, separatorIndex);

                if(separatorIndex + 1 < line.length()){
                    arg = line.substring(separatorIndex + 1);
                    instructions.add(new Pair<>(instruction, arg));
                }
                else
                    instructions.add(new Pair<>(instruction, ""));

            }

        }
        catch (IOException e){
            return instructions;
        }

        return instructions;
    }

}
