package VM;

import javafx.util.Pair;

import java.util.ArrayList;

public class vmMain {

    private static void execute(String fileName){

        ArrayList<Pair<String, String>> program = new ILReader().Read(fileName);

        System.out.println("\tPJP Virtual Machine - running  " + fileName);
        System.out.println("\tPJP Virtual Machine - output:");
        System.out.println();

        new VirtualMachine().Execute(program);

        System.out.println();
        System.out.println("\tPJP Virtual Machine - done");
        System.out.println();
    }


    public static void main(String[] args){

        for (String file : args)
            execute(file);

    }

}
