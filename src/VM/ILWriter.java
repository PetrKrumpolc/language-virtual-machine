package VM;

import javafx.util.Pair;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ILWriter {

    public void Write(ArrayList<Pair<String, String>> program, String fileName){

        try(FileWriter writer = new FileWriter(fileName + ".il")){

            for (Pair<String, String> instruction : program)
                writer.write(instruction.getKey() + " " + instruction.getValue() + System.getProperty("line.separator"));

        }
        catch (IOException e){
            return;
        }

    }

}
