package VM;

import IR.SymbolTable;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class VirtualMachine {

    private ArrayList<Pair<String, String>> _program;
    private Stack<Object>                   _programStack;
    private SymbolTable                     _variables;

    private int                             _instructionPointer;


    public void Execute(ArrayList<Pair<String, String>> instructions){

        _program        = instructions;
        _programStack   = new Stack<>();
        _variables      = new SymbolTable();

        _instructionPointer = 0;

        while (_instructionPointer < _program.size()){

            switch (_program.get(_instructionPointer).getKey()){

                case "add":
                    execAdd();
                    break;
                case "sub":
                    execSub();
                    break;
                case "mul":
                    execMul();
                    break;
                case "div":
                    execDiv();
                    break;
                case "mod":
                    execMod();
                    break;
                case "uminus":
                    execUMINUS();
                    break;
                case "concat":
                    execConcate();
                    break;
                case "and":
                    execAnd();
                    break;
                case "or":
                    execOr();
                    break;
                case "gt":
                    execGT();
                    break;
                case "lt":
                    execLT();
                    break;
                case "eq":
                    execEq();
                    break;
                case "not":
                    execNot();
                    break;
                case "push":
                    execPush(_program.get(_instructionPointer).getValue());
                    break;
                case "load":
                    execLoad(_program.get(_instructionPointer).getValue());
                    break;
                case "save":
                    execSave(_program.get(_instructionPointer).getValue());
                    break;
                case "label":
                    execLabel();
                    break;
                case "jmp":
                    execJMP(_program.get(_instructionPointer).getValue());
                    break;
                case "fjmp":
                    execFJMP(_program.get(_instructionPointer).getValue());
                    break;
                case "print":
                    execPrint(Integer.valueOf(_program.get(_instructionPointer).getValue()));
                    break;
                case "read":
                    execRead(_program.get(_instructionPointer).getValue());
                    break;

            }

            _instructionPointer++;

        }

    }


    private void execAdd(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();


        if(left instanceof Integer && right instanceof Integer)
            _programStack.push((Integer)left + (Integer)right);
        else if(left instanceof Float && right instanceof Float)
            _programStack.push((Float)left + (Float)right);
        else if(left instanceof Integer && right instanceof Float)
            _programStack.push((Integer)left + (Float)right);
        else if(left instanceof Float && right instanceof Integer)
            _programStack.push((Float)left + (Integer)right);

    }

    private void execSub(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();


        if(left instanceof Integer && right instanceof Integer)
            _programStack.push((Integer)left - (Integer)right);
        else if(left instanceof Float && right instanceof Float)
            _programStack.push((Float)left - (Float)right);
        else if(left instanceof Integer && right instanceof Float)
            _programStack.push((Integer)left - (Float)right);
        else if(left instanceof Float && right instanceof Integer)
            _programStack.push((Float)left - (Integer)right);

    }

    private void execMul(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();


        if(left instanceof Integer && right instanceof Integer)
            _programStack.push((Integer)left * (Integer)right);
        else if(left instanceof Float && right instanceof Float)
            _programStack.push((Float)left * (Float)right);
        else if(left instanceof Integer && right instanceof Float)
            _programStack.push((Integer)left * (Float)right);
        else if(left instanceof Float && right instanceof Integer)
            _programStack.push((Float)left * (Integer)right);

    }

    private void execDiv(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();


        if(left instanceof Integer && right instanceof Integer)
            _programStack.push((Integer)left / (Integer)right);
        else if(left instanceof Float && right instanceof Float)
            _programStack.push((Float)left / (Float)right);
        else if(left instanceof Integer && right instanceof Float)
            _programStack.push((Integer)left / (Float)right);
        else if(left instanceof Float && right instanceof Integer)
            _programStack.push((Float)left / (Integer)right);

    }

    private void execMod(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();

        _programStack.push((Integer)left % (Integer)right);
    }

    private void execUMINUS(){

        Object number = _programStack.pop();

        if(number instanceof Integer)
            _programStack.push(-(Integer)number);
        else if(number instanceof Float)
            _programStack.push(-(Float)number);

    }


    private void execConcate(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();

        _programStack.push(left.toString() + right.toString());
    }


    private void execNot(){
        _programStack.push(!(Boolean)_programStack.pop());
    }

    private void execAnd(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();

        _programStack.push((Boolean)left && (Boolean)right);
    }

    private void execOr(){
        Object right    = _programStack.pop();
        Object left     = _programStack.pop();

        _programStack.push((Boolean)left || (Boolean)right);
    }


    private void execEq(){
        _programStack.push(_programStack.pop().equals(_programStack.pop()));
    }

    private void execLT(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();


        if(left instanceof String && right instanceof String)
            _programStack.push(left.toString().compareTo(right.toString()) < 0);
        else if(left instanceof Boolean && right instanceof Boolean)
            _programStack.push(((Boolean)left).compareTo((Boolean) right) < 0);
        else if(left instanceof Integer && right instanceof Integer)
            _programStack.push((Integer)left < (Integer)right);
        else if(left instanceof Float && right instanceof Float)
            _programStack.push((Float)left < (Float)right);
        else if(left instanceof Integer && right instanceof Float)
            _programStack.push((Integer)left < (Float)right);
        else if(left instanceof Float && right instanceof Integer)
            _programStack.push((Float)left < (Integer)right);

    }

    private void execGT(){

        Object right    = _programStack.pop();
        Object left     = _programStack.pop();


        if(left instanceof String && right instanceof String)
            _programStack.push(left.toString().compareTo(right.toString()) > 0);
        else if(left instanceof Boolean && right instanceof Boolean)
            _programStack.push(((Boolean)left).compareTo((Boolean) right) > 0);
        else if(left instanceof Integer && right instanceof Integer)
            _programStack.push((Integer)left > (Integer)right);
        else if(left instanceof Float && right instanceof Float)
            _programStack.push((Float)left > (Float)right);
        else if(left instanceof Integer && right instanceof Float)
            _programStack.push((Integer)left > (Float)right);
        else if(left instanceof Float && right instanceof Integer)
            _programStack.push((Float)left > (Integer)right);

    }


    private void execPush(String arg){
        String type  = arg.substring(0,1);
        String value = arg.substring(1);

        switch (type){
            case "I":
                _programStack.push(Integer.parseInt(value));
                break;
            case "F":
                _programStack.push(Float.parseFloat(value));
                break;
            case "B":
                _programStack.push(Boolean.parseBoolean(value));
                break;
            case "S":
                _programStack.push(value);
                break;
        }
    }

    private void execLoad(String variable){
        _programStack.push(_variables.Get(variable, -1, -1).getValue());
    }

    private void execSave(String variable){

        Object value = _programStack.pop();


        if(!_variables.IsDeclared(variable)){

            if(value instanceof Integer)
                _variables.Add("Int", variable, -1,-1);
            else if(value instanceof Float)
                _variables.Add("Float", variable, -1,-1);
            else if(value instanceof Boolean)
                _variables.Add("Boolean", variable, -1,-1);
            else
                _variables.Add("String", variable, -1,-1);

        }

        _variables.Get(variable, -1, -1).setValue(value);
    }



    private int getLabelPosition(String label){

        for (int i = 0; i < _program.size();i++)
            if(_program.get(i).getKey().equals("label") && _program.get(i).getValue().equals(label))
                return i;

        return -1;
    }

    private void execLabel(){
        //skipping
    }

    private void execJMP(String label){
        _instructionPointer = getLabelPosition(label);
    }

    private void execFJMP(String label){
        if(!(Boolean)_programStack.pop())
            execJMP(label);
    }


    private void execPrint(int count){

        Stack<String> tmp = new Stack<>();

        for(int i = 0; i < count;i++)
            tmp.push(_programStack.pop().toString());

        while (!tmp.isEmpty())
            System.out.print(tmp.pop());

        System.out.println("");

    }

    private void execRead(String type){

        Scanner scanner = new Scanner(System.in);

        System.out.println(type + " input>");
        switch (type) {
            case "String":
                _programStack.push(scanner.next());
                break;
            case "Int":
                _programStack.push(Integer.parseInt(scanner.next()));
                break;
            case "Float":
                _programStack.push(Float.parseFloat(scanner.next()));
                break;
            case "Boolean":
                _programStack.push(Boolean.parseBoolean(scanner.next()));
                break;
        }

    }

}
