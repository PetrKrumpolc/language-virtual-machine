package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class ExpressionStatement extends Statement {
    private Expression _Expression;

    public Expression getExpression() {
        return _Expression;
    }


    public ExpressionStatement(int line, int column, Expression expression){
        super(line, column);

        _Expression = expression;
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {
        _Expression.collectErrors(errors);
    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        return _Expression.buildIL(labelCounter);
    }
}
