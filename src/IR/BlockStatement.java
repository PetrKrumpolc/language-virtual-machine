package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class BlockStatement extends Statement {

    private ArrayList<Statement> _Statements;


    public ArrayList<Statement> getStatements() {
        return _Statements;
    }


    public BlockStatement(int line, int column){
        super(line, column);

        _Statements = new ArrayList<>();
    }


    public void Add(Statement statement){

        if(statement != null)
            _Statements.add(statement);
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        for (Statement statement : _Statements)
            statement.collectErrors(errors);

    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        for (Statement statement : _Statements)
            instructions.addAll(statement.buildIL(labelCounter));

        return instructions;
    }
}
