package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class ReadStatement extends Statement {

    private ArrayList<VariableExpression> _Variables;


    public ArrayList<VariableExpression> getVariables() {
        return _Variables;
    }


    public ReadStatement(int line, int column)
    {
        super(line, column);

        _Variables = new ArrayList<>();
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        for(VariableExpression expression : _Variables)
            expression.collectErrors(errors);

    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        for(VariableExpression variable : _Variables) {
            instructions.add(new Pair<>("read", variable.getType().toString()));
            instructions.add(new Pair<>("save", variable.getName()));
        }

        return instructions;
    }
}
