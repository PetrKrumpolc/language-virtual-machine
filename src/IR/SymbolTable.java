package IR;

import java.util.TreeMap;

public class SymbolTable {

    private TreeMap<String, VariableExpression> table;


    public SymbolTable(){
        table = new TreeMap<>();
    }


    public void Add(String type, String name, int line, int column){

        if(!IsDeclared(name)) {
            VariableExpression value = new VariableExpression(line, column, name);
            value.setType(Type.convert(type));

            table.put(name, value);
        }
        else {
            VariableExpression v = table.get(name);
            CompilerError.Add(String.format("Error: Attempt to declare variable '%s' which is already declared on line: %d and column %d. Line: %d Column: %d",
                                            name,
                                            v.getLine(),
                                            v.getColumn(),
                                            line,
                                            column));
        }
    }


    public boolean IsDeclared(String name){
        return table.containsKey(name);
    }

    public VariableExpression Get(String name, int line, int column){
        if(IsDeclared(name))
            return table.get(name);
        else {
            CompilerError.Add(String.format("Error: Using variable '%s' which is not declared. Line: %d Column: %d", name, line, column));
            return new VariableExpression(line,column, "Error");
        }
    }
}
