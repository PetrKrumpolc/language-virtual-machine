package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class WriteStatement extends Statement {

    private ArrayList<Expression> _Variables;


    public ArrayList<Expression> getVariables() {
        return _Variables;
    }


    public WriteStatement(int line, int column)
    {
        super(line, column);

        _Variables = new ArrayList<>();
    }

    @Override
    public void collectErrors(ArrayList<String> errors) {
        for (Expression expression : _Variables)
            expression.collectErrors(errors);
    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        for(Expression expression : _Variables)
            instructions.addAll(expression.buildIL(labelCounter));

        instructions.add(new Pair<>("print", String.valueOf(_Variables.size())));

        return instructions;
    }
}
