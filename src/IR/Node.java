package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public abstract class Node {

    private int _Line;
    private int _Column;


    public int getLine() {
        return _Line;
    }

    public int getColumn() {
        return _Column;
    }



    public Node(int line, int column){
        _Line   = line;
        _Column = column;
    }


    public abstract void collectErrors(ArrayList<String> errors);

    public abstract ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter);
}
