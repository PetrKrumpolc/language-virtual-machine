package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class ForStatement extends Statement {

    private Expression _Init;
    private Expression _Condition;
    private Expression _Step;
    private Statement  _Body;


    public Expression getInit() {
        return _Init;
    }

    public Expression getCondition() {
        return _Condition;
    }

    public Expression getStep() {
        return _Step;
    }

    public Statement getBody() {
        return _Body;
    }


    public ForStatement(int line, int column, Expression init, Expression condition, Expression step, Statement body){
        super(line, column);

        _Init       = init;
        _Condition  = condition;
        _Step       = step;
        _Body       = body;
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        _Init.collectErrors(errors);
        _Condition.collectErrors(errors);
        _Step.collectErrors(errors);
        _Body.collectErrors(errors);
    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();
        int startLabel  = labelCounter.next();
        int stopLabel   = labelCounter.next();

        instructions.addAll(_Init.buildIL(labelCounter));
        instructions.add(new Pair<>("label", String.valueOf(startLabel)));
        instructions.addAll(_Condition.buildIL(labelCounter));
        instructions.add(new Pair<>("fjmp", String.valueOf(stopLabel)));
        instructions.addAll(_Body.buildIL(labelCounter));
        instructions.addAll(_Step.buildIL(labelCounter));
        instructions.add(new Pair<>("jmp", String.valueOf(startLabel)));
        instructions.add(new Pair<>("label", String.valueOf(stopLabel)));

        return instructions;
    }
}
