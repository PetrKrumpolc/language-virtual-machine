package IR;

public abstract class Expression extends Node {

    private Type _Type;


    public Type getType() {
        return _Type;
    }

    public void setType(Type _Type) {
        this._Type = _Type;
    }


    public Expression(int line, int column){
        super(line, column);

        _Type = Type.Error;
    }

}
