package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class BinExpression extends Expression {

    private String      _Operand;
    private Expression  _Left;
    private Expression  _Right;


    public Expression getRight() {
        return _Right;
    }

    public Expression getLeft() {
        return _Left;
    }

    public String getOperand() {
        return _Operand;
    }


    public BinExpression(int line, int column, Expression left, String operand, Expression right){
        super(line, column);

        _Operand = operand;

        _Left   = left;
        _Right  = right;

        setType(evaluateType(left, operand, right));
    }


    private Type evaluateType(Expression left, String operand, Expression right){

        if(operand.equals("+") || operand.equals("-") || operand.equals("*") || operand.equals("/")){

            if((left.getType() == Type.Float && right.getType() == Type.Float) ||
                    (left.getType() == Type.Int && right.getType() == Type.Int))
                return left.getType();
            else if((left.getType() == Type.Float || left.getType() == Type.Int) &&
                    (right.getType() == Type.Float || right.getType() == Type.Int))
                return Type.Float;

        }
        else if(operand.equals("%")){

            if(left.getType() == Type.Int && right.getType() == Type.Int)
                return Type.Int;

        }
        else if (operand.equals(".")){

            if(left.getType() == Type.String && right.getType() == Type.String)
                return Type.String;

        }
        else if(operand.equals("<") || operand.equals("<=") || operand.equals("==") || operand.equals("!=") || operand.equals(">=") || operand.equals(">")){

            if(left.getType() == right.getType())
                return Type.Boolean;
            else if((left.getType() == Type.Float || left.getType() == Type.Int) &&
                    (right.getType() == Type.Float || right.getType() == Type.Int))
                return Type.Boolean;

        }
        else if(operand.equals("&&") || operand.equals("||")){

            if(left.getType() == Type.Boolean && right.getType() == Type.Boolean)
                return Type.Boolean;

        }

        return Type.Error;
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        _Left.collectErrors(errors);
        _Right.collectErrors(errors);

        if(_Operand.equals("+") || _Operand.equals("-") || _Operand.equals("*") || _Operand.equals("/")){

            if(_Left.getType() == Type.Boolean || _Left.getType() == Type.String || _Right.getType() == Type.Boolean || _Right.getType() == Type.String)
                errors.add(String.format("Error: %s is not defined on types %s and %s. Line: %d Column: %d",
                                         _Operand,
                                         _Left.getType().toString(),
                                         _Right.getType().toString(),
                                         getLine(),
                                         getColumn()));
        }
        else if(_Operand.equals("%")){

            if(_Left.getType() != Type.Int)
                errors.add(String.format("Error: l-value of mod operator must be Int. Line: %d Column: %d", _Left.getLine(), _Left.getColumn()));
            if(_Right.getType() != Type.Int)
                errors.add(String.format("Error: r-value of mod operator must be Int. Line: %d Column: %d", _Right.getLine(), _Right.getColumn()));

        }
        else if (_Operand.equals(".")){

            if(_Left.getType() != Type.String || _Right.getType() != Type.String)
                errors.add(String.format("Error: '.' (string concatenation) Invalid types to concatenate. %s and %s. Line: %d Column: %d",
                                         _Left.getType().toString(),
                                         _Right.getType().toString(),
                                         getLine(),
                                         getColumn()));

        }
        else if(_Operand.equals("<") || _Operand.equals("<=") || _Operand.equals("==") || _Operand.equals("!=") || _Operand.equals(">=") || _Operand.equals(">")){

            if(_Left.getType() != _Right.getType())
                if((_Right.getType() != Type.Float && _Left.getType() != Type.Int) &&
                    (_Left.getType() != Type.Float && _Right.getType() != Type.Int))
                errors.add(String.format("Error: %s is not defined on types %s and %s. Line: %d Column: %d",
                                         _Operand,
                                         _Left.getType().toString(),
                                         _Right.getType().toString(),
                                         getLine(),
                                         getColumn()));

        }
        else if(_Operand.equals("&&") || _Operand.equals("||")){

            if(_Left.getType() != Type.Boolean)
                errors.add(String.format("Error: l-value must be Boolean. Line: %d Column: %d", _Left.getLine(), _Left.getColumn()));

            if(_Right.getType() != Type.Boolean)
                errors.add(String.format("Error: r-value must be Boolean. Line: %d Column: %d", _Left.getLine(), _Left.getColumn()));

        }

    }


    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> left  = _Left.buildIL(labelCounter);
        ArrayList<Pair<String, String>> right = _Right.buildIL(labelCounter);

        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        switch (_Operand){
            case "+":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("add", ""));
                break;
            case "-":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("sub", ""));
                break;
            case "*":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("mul", ""));
                break;
            case "/":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("div", ""));
                break;
            case "%":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("mod", ""));
                break;
            case ".":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("concat", ""));
                break;
            case "<":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("lt", ""));
                break;
            case "<=":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("lt", ""));
                instructions.add(new Pair<>("not", ""));
                instructions.add(new Pair<>("fjmp", String.valueOf(labelCounter.next())));
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("eq", ""));
                instructions.add(new Pair<>("label", String.valueOf(labelCounter.getActual())));
                break;
            case ">=":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("gt", ""));
                instructions.add(new Pair<>("not", ""));
                instructions.add(new Pair<>("fjmp", String.valueOf(labelCounter.next())));
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("eq", ""));
                instructions.add(new Pair<>("label", String.valueOf(labelCounter.getActual())));
                break;
            case ">":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("gt", ""));
                break;
            case "==":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("eq", ""));
                break;
            case "!=":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("eq", ""));
                instructions.add(new Pair<>("not", ""));
                break;
            case "&&":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("and", ""));
                break;
            case "||":
                instructions.addAll(left);
                instructions.addAll(right);
                instructions.add(new Pair<>("or", ""));
                break;
        }

        return instructions;
    }
}
