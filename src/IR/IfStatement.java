package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class IfStatement extends Statement {

    private Expression  _Condition;
    private Statement   _Then;
    private Statement   _Else;


    public Expression getCondition() {
        return _Condition;
    }

    public Statement getThen() {
        return _Then;
    }

    public Statement getElse() {
        return _Else;
    }

    public void setElse(Statement value){
        _Else = value;
    }


    public IfStatement(int line, int column, Expression condition, Statement then, Statement _else){
        super(line, column);

        _Condition  = condition;
        _Then       = then;
        _Else       = _else;
    }

    public  IfStatement(int line, int column, Expression condition, Statement then) {
        super(line, column);

        _Condition  = condition;
        _Then       = then;
        _Else       = null;
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        _Condition.collectErrors(errors);
        _Then.collectErrors(errors);
        _Else.collectErrors(errors);
    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();
        int elseLabel   = labelCounter.next();

        instructions.addAll(_Condition.buildIL(labelCounter));

        if(_Else == null){
            instructions.add(new Pair<>("fjmp", String.valueOf(elseLabel)));
            instructions.addAll(_Then.buildIL(labelCounter));
            instructions.add(new Pair<>("label", String.valueOf(elseLabel)));
        }
        else {
            int endLabel    = labelCounter.next();

            instructions.add(new Pair<>("fjmp", String.valueOf(elseLabel)));
            instructions.addAll(_Then.buildIL(labelCounter));
            instructions.add(new Pair<>("jmp", String.valueOf(endLabel)));
            instructions.add(new Pair<>("label", String.valueOf(elseLabel)));
            instructions.addAll(_Else.buildIL(labelCounter));
            instructions.add(new Pair<>("label", String.valueOf(endLabel)));
        }

        return instructions;
    }
}
