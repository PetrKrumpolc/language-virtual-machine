package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class UnaryExpression extends Expression {

    private String      _Operand;
    private Expression  _Value;


    public String getOperand() {
        return _Operand;
    }

    public Expression getValue() {
        return _Value;
    }


    public  UnaryExpression(int line, int column, String operand, Expression value){
        super(line, column);

        _Operand    = operand;
        _Value      = value;

        setType(evaluateType(operand, value));
    }


    private Type evaluateType(String operand, Expression value){

        if(operand.equals("!")){

            if(value.getType() == Type.Boolean)
                return Type.Boolean;

        }
        else if(operand.equals("-")){

            if(value.getType() == Type.Int || value.getType() == Type.Float)
                return value.getType();

        }

        return Type.Error;
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        _Value.collectErrors(errors);

        if(_Operand.equals("!")){

            if(_Value.getType() != Type.Boolean)
                errors.add(String.format("Error: ! (negation) can be used on boolean expression. Line: %d Column: %d", getLine(), getColumn()));

        }
        else if(_Operand.equals("-")){

            if(!(_Value.getType() == Type.Int || _Value.getType() == Type.Float))
                errors.add(String.format("Error: - (minus) can be used on number expression. Line: %d Column: %d", getLine(), getColumn()));

        }

    }


    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        instructions.addAll(_Value.buildIL(labelCounter));

        if(_Operand.equals("!"))
            instructions.add(new Pair<>("not", ""));
        else if(_Operand.equals("-"))
            instructions.add(new Pair<>("uminus", ""));
        else
            instructions.add(new Pair<>("NOP", ""));

        return instructions;
    }
}
