package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class TernaryExpression extends Expression {

    private Expression _Condition;
    private Expression _Left;
    private Expression _Right;


    public Expression getCondition() {
        return _Condition;
    }

    public Expression getLeft() {
        return _Left;
    }

    public Expression getRight() {
        return _Right;
    }


    public  TernaryExpression(int line, int column, Expression condition, Expression left, Expression right)
    {
        super(line, column);

        _Condition  = condition;
        _Left       = left;
        _Right      = right;

        setType(evaluateType(condition, left, right));
    }

    private Type evaluateType(Expression condition, Expression left, Expression right){

        if(condition.getType() != Type.Boolean)
            return Type.Error;

        if(left.getType() != right.getType())
            return Type.Error;
        else
            return left.getType();
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        _Condition.collectErrors(errors);
        _Left.collectErrors(errors);
        _Right.collectErrors(errors);

        if(_Condition.getType() != Type.Boolean)
            errors.add(String.format("Error: Conditional expression must return boolean value. Line: %d Column: %d", getLine(), getColumn()));

        if(_Left.getType() != _Right.getType())
            errors.add(String.format("Error: Consequent and alternative part must return same Type. Line: %d Column: %d", getLine(), getColumn()));
    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();
        int label = labelCounter.next();

        instructions.addAll(_Condition.buildIL(labelCounter));
        instructions.add(new Pair<>("fjmp", String.valueOf(label)));
        instructions.addAll(_Left.buildIL(labelCounter));
        instructions.add(new Pair<>("label", String.valueOf(label)));
        instructions.addAll(_Right.buildIL(labelCounter));

        return instructions;
    }
}
