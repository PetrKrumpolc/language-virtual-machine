package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class LiteralExpression extends Expression {

    private Object _Value;


    public Object getValue() {
        return _Value;
    }


    public  LiteralExpression(int line, int column, Object value){
        super(line, column);

        _Value = value;
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        switch (getType()){
            case Int:
                instructions.add(new Pair<>("push", "I" + _Value.toString()));
                break;
            case Float:
                instructions.add(new Pair<>("push", "F" + _Value.toString()));
                break;
            case Boolean:
                instructions.add(new Pair<>("push", "B" + _Value.toString()));
                break;
            case String:
                instructions.add(new Pair<>("push", "S" + _Value.toString().replaceAll("\"", "")));
                break;
                default:
                    instructions.add(new Pair<>("NOP", ""));
        }

        return instructions;
    }
}
