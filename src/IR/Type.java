package IR;

public enum Type {
    Int,
    String,
    Float,
    Boolean,
    Error;

    public static Type convert(String name){
        if(name.equals("boolean"))
            return Type.Boolean;
        else if(name.equals("int"))
            return Type.Int;
        else if (name.equals("float"))
            return Type.Float;
        else if (name.equals("String"))
            return Type.String;
        else
            return Type.Error;
    }
}
