package IR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CompilerError {

    private static ArrayList<String> _Errors = new ArrayList<>();;

    public static List<String> getErrors(){
        return Collections.unmodifiableList(_Errors);
    }


    public static void Add(String error){
        _Errors.add(error);
    }

    public static boolean HasErrors(){
        return !_Errors.isEmpty();
    }
}
