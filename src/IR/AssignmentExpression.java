package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class AssignmentExpression extends Expression {

    private VariableExpression  _Variable;
    private Expression          _Right;



    public VariableExpression getVariable() {
        return _Variable;
    }

    public Expression getRight() {
        return _Right;
    }


    public  AssignmentExpression(int line, int column, VariableExpression variable, Expression expression)
    {
        super(line, column);

        _Variable   = variable;
        _Right      = expression;

        setType(evaluateType(variable, expression));
    }


    private Type evaluateType(VariableExpression variable, Expression expression)
    {
        if(variable.getType() == expression.getType())
            return variable.getType();
        else if(variable.getType() == Type.Float && expression.getType() == Type.Int) {
            _Right.setType(Type.Float);
            return Type.Float;
        }
        else
            return Type.Error;
    }


    @Override
    public void collectErrors(ArrayList<String> errors) {

        _Variable.collectErrors(errors);
        _Right.collectErrors(errors);


        if(_Variable.getType() != _Right.getType()){

            if (_Variable.getType() != Type.Float && _Right.getType() != Type.Int)
                errors.add(String.format("Error: Invalid assignment. Types mismatch. Line: %d Column: %d", getLine(), getColumn()));
        }
    }


    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {

        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        instructions.addAll(_Right.buildIL(labelCounter));
        instructions.add(new Pair<>("save", _Variable.getName()));

        return instructions;
    }
}
