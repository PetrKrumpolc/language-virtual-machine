package IR;

import javafx.util.Pair;

import java.util.ArrayList;

public class VariableExpression extends Expression {

    private String _Name;
    private Object _Value;


    public String getName() {
        return _Name;
    }


    public Object getValue() {
        return _Value;
    }

    public void setValue(Object _Value) {
        this._Value = _Value;
    }



    public  VariableExpression(int line, int column, String name)
    {
        super(line, column);
        _Name   = name;
        _Value  = null;
    }

    @Override
    public void collectErrors(ArrayList<String> errors) {

    }

    @Override
    public ArrayList<Pair<String, String>> buildIL(LabelCounter labelCounter) {
        ArrayList<Pair<String, String>> instructions = new ArrayList<>();

        instructions.add(new Pair<>("load", _Name));

        return instructions;
    }
}
