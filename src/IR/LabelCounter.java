package IR;

public class LabelCounter {

    private int _Counter;


    public int getActual(){
        return _Counter;
    }

    public int next(){
        return ++_Counter;
    }


    public LabelCounter(){
        _Counter = 0;
    }
}
