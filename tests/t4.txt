if (3<4) write "Podminka funguje";
else {
 write "Podminka nefunguje";
}

var i : int;

for(i=0; i<10; i=i+1) {
 write "i=",i;
 i=i+1;
}

write "";

write "<Vstup dat - a,b#,c$>";
var a : int;
var b : float;
var c : String; 
var e: boolean;
a = 0;
b = 0.0;
c = "";
e = True;
read a,b,c,e;
write "a,b,c,e: ", a, ",", b, ",", c, ",",e;

write "";

write "<Vyrazy>";
write "2+3*5(17): ",2+3*5;
write "17 / 3(5): ", 17 / 3;
write "17 mod 3(2): ", 17 % 3;
write "2.5*2.5/6.25(1.0): ", 2.5*2.5/6.25;
write "1.5*3(4.5): ", 1.5*3;
write "abc+def (abcdef): ", "abc"."def";
write "";

write "<Konstanty>";
write "10: ",10;
write " 1.25: ", 1.25;
write "";;

write "<Promenne>";
var s : String;
s="Abcd";
write "s(Abcd): ", s;